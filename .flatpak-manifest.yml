# SPDX-FileCopyrightText: 2022 George Florea Bănuș <georgefb899@gmail.com>
# SPDX-License-Identifier: CC0-1.0
---
app-id: org.kde.haruna
runtime: org.kde.Platform
runtime-version: '5.15-22.08'
sdk: org.kde.Sdk
command: haruna
rename-icon: haruna
copy-icon: true
finish-args:
  - '--share=ipc'
  - '--share=network'
  - '--socket=fallback-x11'
  - '--socket=wayland'
  - '--socket=pulseaudio'
  - '--device=dri'
  - '--filesystem=host'
  - '--talk-name=org.freedesktop.ScreenSaver'
  - '--own-name=org.mpris.MediaPlayer2.haruna'
  - '--env=LC_NUMERIC=C'
modules:
  - name: haruna
    buildsystem: cmake-ninja
    sources:
      - type: dir
        path: '.'
    modules:
      - name: libmpv
        cleanup:
          - /include
          - /lib/pkgconfig
          - /share/man
        # buildsystem: meson
        # config-opts:
        #   - '-Dlua=enabled'
        #   - '-Dlibmpv=true'
        buildsystem: simple
        build-commands:
          - python3 waf configure --prefix=/app --enable-libmpv-shared --disable-cplayer --disable-build-date --disable-alsa
          - python3 waf build
          - python3 waf install
        sources:
          - type: archive
            url: 'https://github.com/mpv-player/mpv/archive/refs/tags/v0.35.1.tar.gz'
            sha256: '41df981b7b84e33a2ef4478aaf81d6f4f5c8b9cd2c0d337ac142fc20b387d1a9'
          - type: file
            url: 'https://waf.io/waf-2.0.25'
            sha256: '21199cd220ccf60434133e1fd2ab8c8e5217c3799199c82722543970dc8e38d5'
            dest-filename: waf
        modules:
          - name: luajit
            no-autogen: true
            cleanup:
              - /bin
              - /lib/*.a
              - /include
              - /lib/pkgconfig
              - /share/man
            sources:
              - type: archive
                url: 'https://luajit.org/download/LuaJIT-2.1.0-beta3.tar.gz'
                sha256: '1ad2e34b111c802f9d0cdf019e986909123237a28c746b21295b63c9e785d9c3'
              - type: shell
                commands:
                  - sed -i 's|/usr/local|/app|' ./Makefile

          - name: libv4l2
            cleanup:
              - /sbin
              - /bin
              - /include
              - /lib/*.la
              - /lib/*/*.la
              - /lib*/*/*/*.la
              - /lib/pkgconfig
              - /share/man
            config-opts:
              - '--disable-static'
              - '--disable-bpf'
              - '--with-udevdir=/app/lib/udev'
            sources:
              - type: archive
                url: 'https://linuxtv.org/downloads/v4l-utils/v4l-utils-1.22.1.tar.bz2'
                sha256: '65c6fbe830a44ca105c443b027182c1b2c9053a91d1e72ad849dfab388b94e31'

          - name: nv-codec-headers
            cleanup:
              - '*'
            no-autogen: true
            make-install-args:
              - PREFIX=/app
            sources:
              - type: archive
                url: 'https://github.com/FFmpeg/nv-codec-headers/releases/download/n11.1.5.2/nv-codec-headers-11.1.5.2.tar.gz'
                sha256: '1442e3159e7311dd71f8fca86e615f51609998939b6a6d405d6683d8eb3af6ee'

          - name: ffmpeg
            cleanup:
              - /include
              - /lib/pkgconfig
              - /share/ffmpeg/examples
            config-opts:
              - '--enable-shared'
              - '--disable-static'
              - '--enable-gnutls'
              - '--enable-gpl'
              - '--disable-doc'
              - '--disable-programs'
              - '--disable-encoders'
              - '--disable-muxers'
              - '--enable-encoder=png,libwebp'
              - '--enable-libv4l2'
              - '--enable-libdav1d'
              - '--enable-libfontconfig'
              - '--enable-libfreetype'
              - '--enable-libopus'
              - '--enable-librsvg'
              - '--enable-libvpx'
              - '--enable-libmp3lame'
              - '--enable-libwebp'
            sources:
              - type: archive
                url: 'https://ffmpeg.org/releases/ffmpeg-5.1.2.tar.gz'
                sha256: '87fe8defa37ce5f7449e36047171fed5e4c3f4bb73eaccea8c954ee81393581c'

          - name: libass
            cleanup:
              - /include
              - /lib/*.la
              - /lib/pkgconfig
            config-opts:
              - '--disable-static'
            sources:
              - type: archive
                url: 'https://github.com/libass/libass/releases/download/0.17.0/libass-0.17.0.tar.gz'
                sha256: '72b9ba5d9dd1ac6d30b5962f38cbe7aefb180174f71d8b65c5e3c3060dbc403f'

          - name: uchardet
            buildsystem: cmake-ninja
            config-opts:
              - '-DCMAKE_BUILD_TYPE=Release'
              - '-DBUILD_STATIC=0'
            cleanup:
              - /bin
              - /include
              - /lib/pkgconfig
              - /share/man
            sources:
              - type: archive
                url: 'https://gitlab.freedesktop.org/uchardet/uchardet/-/archive/v0.0.8/uchardet-v0.0.8.tar.gz'
                sha256: '5aa402a1b5b1dbb8d81096f141ff1224e079f4d3a1db0f79ecca782756d3a416'

      - name: yt-dlp
        no-autogen: true
        no-make-install: true
        make-args:
          - yt-dlp
          - PYTHON=/usr/bin/python3
        post-install:
          - install yt-dlp /app/bin
        sources:
          - type: archive
            url: 'https://github.com/yt-dlp/yt-dlp/archive/refs/tags/2023.01.06.tar.gz'
            sha256: '1ac3dfed834fec9ae18e8d2f0ee8be561df172d08ee42d90423f99fedd7f3226'
